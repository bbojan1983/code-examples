<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class CommandController extends ApiController
{

	/**
     * Handle API request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request)
    {
        try {
            return $request->api()->handle();
        } catch (\Exception $e) {
            return $this->setStatusCode($e->getCode())->respondWithError($e->getMessage());
        }
    }

}
